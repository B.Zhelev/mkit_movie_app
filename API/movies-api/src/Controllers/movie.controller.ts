// /blog-backend/src/blog/blog.controller.ts
import { 
    Controller, 
    Get, 
    Param, 
    Post, 
    Body, 
    Put, 
    Query, 
    Delete } from '@nestjs/common';
import { CommentService } from '../Services/comment.service';
import { MovieService } from '../Services/movie.service';
import { CreateCommentDTO } from '../DTOs/createComment.dto';
import { RatingService } from 'src/Services/rating.service';
import { FavoriteService } from 'src/Services/favorite.service';
import { MovieDTO } from 'src/DTOs/movie.dto';
import { IMovie } from 'src/Interfaces/movie.interface';
import { UpdateCommentDTO } from 'src/DTOs/updateComment.dto';
import { RatingDTO } from 'src/DTOs/rating.dto';
import { FavoriteDTO } from 'src/DTOs/favorites.dto';
import { ApiCreatedResponse, ApiTags } from '@nestjs/swagger';
import { IComment } from 'src/Interfaces/comment.interface';

@ApiTags('movies')
@Controller('movies')
export class MovieController {

  constructor(
    private readonly commentService: CommentService,
    private readonly movieService: MovieService,
    private readonly ratingService: RatingService,
    private readonly favoriteService: FavoriteService
  ) { }

//MOVIES
  @Get()
  @ApiCreatedResponse({
    description: 'Returns all movies matching search term',
    type: [IMovie]
  })
  async getAll(
    @Query('searchTerm') search: string, 
    @Query('user') user: string
    ): Promise<IMovie[]> {
    const searchMovieObj: MovieDTO = {search, user};
    return await this.movieService.getMovies(searchMovieObj)
  }

  @Get(':id')
  @ApiCreatedResponse({
    description: 'Returns single movie based on ID',
    type: IMovie
  })
  async getByID(
    @Param('id') id: string | number,
    @Query('user') user: string
    ): Promise<IMovie> {
      id = +id;
    const searchSingleMovieObj: MovieDTO = {id, user};
    return await this.movieService.getSingleMovie(searchSingleMovieObj)
  }

//COMMENTS

  @Post(':id/comment')
  @ApiCreatedResponse({
    description: 'Returns the created comment',
    type: IComment
  })
  async postComment( 
    @Body()commentObjToSend :CreateCommentDTO
    ){
      console.log(commentObjToSend)
    return await this.commentService.addComment(commentObjToSend)
  }

  @Delete(':id/comment') 
  @ApiCreatedResponse({
    description: 'Returns id of the movie from which the comment was deleted and the commentId',
    type: Object
  })
    async delComment(
      @Body() request: UpdateCommentDTO
    ){

      return await this.commentService.delComment(request)
    }
  
  @Put(':id/comment')
  @ApiCreatedResponse({
    description: 'Updates a given comment and returns it',
    type: IComment
  })
  async updateComment(
    @Body() commentObjToSend: UpdateCommentDTO
  ){

    return await this.commentService.updateComment(commentObjToSend)
  }

//RATING 
 @Post(':id/rate')
 @ApiCreatedResponse({
  description: 'Adds rating to movie and return movieId along with rating value',
  type: Object
})
 async rateMovie(
   @Body()ratingObjToSend: RatingDTO
 ){

  return await this.ratingService.rateMovie(ratingObjToSend);
 }

 @Put(':id/rate')
 @ApiCreatedResponse({
  description: 'Modifies rating to movie and return movieId along with rating value',
  type: Object
})
 async changeRating(
  @Body()ratingObjToSend: RatingDTO
){

  return await this.ratingService.rateMovie(ratingObjToSend);
}

//FAVORITES 
@Post(':id/favorites')
@ApiCreatedResponse({
  description: 'Adds movie to favorites and returns it',
  type: IMovie
})
async addToFavorites(
  @Body() FavoriteObjToSend: FavoriteDTO
){

 return await this.favoriteService.addToFavorites(FavoriteObjToSend)
}

@Delete(':id/favorites')
@ApiCreatedResponse({
  description: 'Removes a movie from favorites and returns it',
  type: IMovie
})
async removeFromFavorites(
  @Body() FavoriteObjToSend: FavoriteDTO
){

  return await this.favoriteService.removeFromFavorites(FavoriteObjToSend)
}
}