import { Controller, Get, Param } from '@nestjs/common';
import { ApiTags, ApiCreatedResponse } from '@nestjs/swagger';
import { IMovie } from 'src/Interfaces/movie.interface';
import { FavoriteService } from 'src/Services/favorite.service';
@ApiTags('user')
@Controller('user')
export class UserController {
  constructor(
    private readonly favoriteService: FavoriteService
  ) {}

  /**
   * Gets user's favorites with rating
   * @param userName username of user
   */
  @Get(':user_name')
  @ApiCreatedResponse({
    description: 'Returns all movies favorite movies for user',
    type: [IMovie]
  })
 async getUser(
  @Param('user_name') userName
 ){
   
  return await this.favoriteService.getFavoritesForUser(userName)
 }
}
