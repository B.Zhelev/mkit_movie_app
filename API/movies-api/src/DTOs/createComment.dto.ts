import { ApiProperty } from "@nestjs/swagger";

export class CreateCommentDTO {
  @ApiProperty()
  readonly comment: string;
  @ApiProperty()
  readonly movie_id: number;
  @ApiProperty()
  readonly owner: string;
}