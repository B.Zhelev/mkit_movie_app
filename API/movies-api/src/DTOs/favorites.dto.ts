import { ApiProperty } from "@nestjs/swagger";

export class FavoriteDTO {
  @ApiProperty()
  readonly movie_id: number;
  @ApiProperty()
  readonly owner: string;
}