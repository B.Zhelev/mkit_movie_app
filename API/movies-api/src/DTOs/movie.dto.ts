import { ApiProperty } from '@nestjs/swagger';

export class MovieDTO {
    @ApiProperty()
    readonly search?: string;
    @ApiProperty()
    readonly id?: number;
    @ApiProperty()
    readonly user?: string;
}