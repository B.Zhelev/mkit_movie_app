import { ApiProperty } from "@nestjs/swagger";

export class RatingDTO {
    @ApiProperty()
    readonly value: number;
    @ApiProperty()
    readonly owner: string; 
    @ApiProperty()
    readonly movie_id: number;
}