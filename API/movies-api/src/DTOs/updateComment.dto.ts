import { ApiProperty } from "@nestjs/swagger";

export class UpdateCommentDTO {
    @ApiProperty()
    readonly comment: string;
    @ApiProperty()
    readonly commentId: string;
    @ApiProperty()
    readonly owner: string;
    @ApiProperty()
    readonly movie_id: number;
  }