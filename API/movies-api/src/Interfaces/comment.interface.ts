import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

export class IComment extends Document {
  @ApiProperty()
  readonly comment: string;
  @ApiProperty()
  readonly owner: string;
  @ApiProperty()
  readonly date_posted: string;
  @ApiProperty()
  readonly movie_id: number;
}