import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

export class IFavorite extends Document {
  @ApiProperty()
  readonly favorites: number[];
  @ApiProperty()
  readonly owner: string;
}