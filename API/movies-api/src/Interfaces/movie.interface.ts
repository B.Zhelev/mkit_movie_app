import { IComment } from "./comment.interface";
import {ApiProperty} from '@nestjs/swagger';
export class IMovie {
    @ApiProperty()
    readonly id: number;

    @ApiProperty()
    readonly name: string;

    @ApiProperty()
    readonly genres: string[];

    @ApiProperty()
    readonly runtime: number;

    @ApiProperty()
    readonly premiered: string; 

    @ApiProperty()
    readonly officialSite: string | null; 

    @ApiProperty()
    readonly image: Object;

    @ApiProperty()
    readonly summary: string;

    @ApiProperty()
    readonly comments?: IComment[];

    @ApiProperty()
    readonly rating?: number
}