import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

export class IRating extends Document {
  @ApiProperty()
  readonly movie_id: number;
  @ApiProperty()
  readonly value: number;
  @ApiProperty()
  readonly owner: string;
}