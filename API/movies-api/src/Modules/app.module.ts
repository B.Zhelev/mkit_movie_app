import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose'; 
import { MovieModule } from './movie.module';
import * as dotenv from 'dotenv';
import { UserModule } from './user.module';
import { AuthMiddleware } from 'src/Middleware/auth.middleware';

dotenv.config()
@Module({
  imports: [
    MovieModule,
    UserModule,
    MongooseModule.forRoot(`mongodb+srv://${process.env.MONGO_USR_NAME}:${process.env.MONGO_PASS}@cluster0.pw4e8.mongodb.net/${process.env.MONGO_APP}?retryWrites=true&w=majority`, { useNewUrlParser: true })
  ] 
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): MiddlewareConsumer | void {
    consumer
      .apply(AuthMiddleware)
      .exclude('/movies','/movies/:id')
      .forRoutes('*')
  }
}
