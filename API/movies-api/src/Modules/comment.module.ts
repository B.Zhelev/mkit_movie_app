//needed
import {Module} from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
//services
import { CommentService } from '../Services/comment.service';
//schemas
import { CommentSchema } from '../Schemas/comment.schema';


@Module({
    imports: [MongooseModule.forFeature([{ name: 'Comment', schema: CommentSchema }])],
    providers: [CommentService],
    exports: [CommentService]
  })
export class CommentModule {}