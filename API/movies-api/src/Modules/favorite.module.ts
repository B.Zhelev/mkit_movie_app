//needed
import {Module, forwardRef} from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
//services
import { FavoriteService } from 'src/Services/favorite.service';
//schemas
import { FavoriteSchema } from 'src/Schemas/favorite.schema';
//other modules
import { MovieModule } from './movie.module';



@Module({
    imports: [MongooseModule.forFeature([{ name: 'Favorite', schema: FavoriteSchema }]), forwardRef(() => MovieModule)],
    providers: [FavoriteService],
    exports: [FavoriteService]
  })
export class FavoriteModule {}