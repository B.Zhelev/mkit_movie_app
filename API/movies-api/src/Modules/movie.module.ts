//needed
import {Module, forwardRef} from '@nestjs/common';
import {HttpModule} from '@nestjs/common';
//services
import { MovieService } from '../Services/movie.service';
//controllers
import { MovieController } from '../Controllers/movie.controller';
//other modules
import { CommentModule } from './comment.module';
import { RatingModule } from './rating.module';
import { FavoriteModule } from './favorite.module';

@Module({
  imports: [HttpModule, CommentModule, RatingModule, forwardRef(() => FavoriteModule)],
  providers: [MovieService],
  controllers: [MovieController],
  exports: [MovieService]

  })
export class MovieModule {}