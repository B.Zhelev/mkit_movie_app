//needed
import {Module} from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
//services
import { RatingService } from '../Services/rating.service';
//schemas
import { RatingSchema } from '../Schemas/rating.schema';


@Module({
    imports: [MongooseModule.forFeature([{ name: 'Rating', schema: RatingSchema }])],
    providers: [RatingService],
    exports: [RatingService]
  })
export class RatingModule {}