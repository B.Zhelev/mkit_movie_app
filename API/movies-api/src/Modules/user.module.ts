//needed
import {Module } from '@nestjs/common';
//controllers
import { UserController } from 'src/Controllers/user.controller';
//other modules
import { FavoriteModule } from './favorite.module';

@Module({
    imports: [FavoriteModule],     
    controllers: [UserController],
})
export class UserModule {}