import * as mongoose from 'mongoose';

export const CommentSchema = new mongoose.Schema({
  comment: String,
  owner: String,
  movie_id: Number,
});