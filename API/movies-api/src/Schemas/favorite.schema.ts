import * as mongoose from 'mongoose';


export const FavoriteSchema = new mongoose.Schema({
  favorites: [Number],
  owner: String,
});