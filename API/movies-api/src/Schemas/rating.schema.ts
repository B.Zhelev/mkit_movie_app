import * as mongoose from 'mongoose';

export const RatingSchema = new mongoose.Schema({
  value: Number,
  owner: String,
  movie_id: Number,
});