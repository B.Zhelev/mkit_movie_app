// /blog-backend/src/blog/blog.service.ts
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { IComment } from '../Interfaces/comment.interface';
import { CreateCommentDTO } from '../DTOs/createComment.dto';
import { UpdateCommentDTO } from 'src/DTOs/updateComment.dto';

@Injectable()
export class CommentService {
  constructor(@InjectModel('Comment') private readonly commentModel: Model<IComment>) { }

  //POST
  /**
   * Creates a comment 
   * @param {CreateCommentDTO} commentObj comment object containing movie id, comment text, user and date posted
   */
  async addComment(commentObj: CreateCommentDTO): Promise<IComment> {
    const newComment = await new this.commentModel(commentObj);

    return newComment.save();
  }  

  //GET 
  /**
   * Retrives comments for movie
   * @param {number} id comment id
   * @param {string} owner owner of comments
   */
  async getCommentsForMovie(id: number, owner: string): Promise<IComment[]> {

    return await this.commentModel.find({
      movie_id: {$eq: id},
      owner: {$eq: owner}
    })
  }

  //DELETE
  /**
   * Deletes a comment 
   * @param {string} commentId Id of comment
   */
  async delComment(request: UpdateCommentDTO): Promise<any>{

    await this.commentModel.deleteOne({
      _id: {$eq: request.commentId},
      owner: {$eq: request.owner},
      movie_id: {$eq: request.movie_id}
    });

    return {commentId: request.commentId, movie_id: request.movie_id};
  }

  //PUT 
  /**
   * Updates a comment
   * @param {UpdateCommentDTO} commentObj comment object containing comment id and comment text
   */
  async updateComment(commentObj: UpdateCommentDTO): Promise<any> {

    await this.commentModel.updateOne(
      {_id: {$eq: commentObj.commentId},
      owner: {$eq: commentObj.owner},
      movie_id: {$eq: commentObj.movie_id}},
      {$set: {comment: commentObj.comment}})

      return await this.commentModel.findById(commentObj.commentId)
  }
} 