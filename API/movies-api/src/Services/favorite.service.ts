import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { FavoriteDTO } from 'src/DTOs/favorites.dto';
import { MovieDTO } from 'src/DTOs/movie.dto';
import { IFavorite } from 'src/Interfaces/favorites.interface';
import { MovieService } from './movie.service';

@Injectable()
export class FavoriteService {
  constructor(
    @InjectModel('Favorite') private readonly favoriteModel: Model<IFavorite>,
    private readonly movieService: MovieService
    ) { }

  /**
   * Adds movie to favorites
   * @param {FavoriteDTO} favoriteObj Contains ID of movie and user
   */
   async addToFavorites(favoriteObj: FavoriteDTO){

    await this.favoriteModel.updateOne(
      {owner: {$eq: favoriteObj.owner}},
      {$addToSet: {favorites: favoriteObj.movie_id }},
      {upsert:true})

      return await this.movieService.getSingleMovie({id: favoriteObj.movie_id, user: favoriteObj.owner});
  }
  
  /**
   * Returns favorite movies for user
   * @param owner 
   */
  async getFavoritesForUser(owner: string){

    const rawData = await this.favoriteModel.findOne({owner: {$eq: owner}})
    try{return Promise.all(rawData.favorites.map(async id => {
      const singleMovieObj: MovieDTO = {id, user: owner}
      return await this.movieService.getSingleMovie(singleMovieObj)
    }))}catch(e){return []}
  }

  /**
   * Removes a movie from user's favorites
   * @param {FavoriteDTO} favoriteObj Contains ID of movie and user
   */
  async removeFromFavorites(favoriteObj: FavoriteDTO){
    
  await this.favoriteModel.updateOne(
      {owner: {$eq: favoriteObj.owner}},
      {$pull: {favorites: favoriteObj.movie_id}}
    )

   return await this.movieService.getSingleMovie({id: favoriteObj.movie_id, user: favoriteObj.owner})
  }

}
