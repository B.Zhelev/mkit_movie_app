import { Injectable } from '@nestjs/common';
import {HttpService} from '@nestjs/common';
import { MovieDTO } from 'src/DTOs/movie.dto';
import {IMovie} from '../Interfaces/movie.interface'
import { CommentService } from './comment.service';
import { RatingService } from './rating.service';


@Injectable() 
export class MovieService {
  constructor(
    private readonly httpService: HttpService,
    private readonly commentService: CommentService,
    private readonly ratingService: RatingService
    ) {}

  //GET

  /**
   * Returns all movies based on a search query, options can be added to add Rating and Comments to movies
   * @param {SearchMovieDTO} searchMovieObj contains search query, serch options and user
   */
  async getMovies(searchMovieObj: MovieDTO): Promise<IMovie[]> {
    const rawMovies: any[] = (await this.httpService.get(`http://api.tvmaze.com/search/shows?q=${searchMovieObj.search}`).toPromise()).data;

    let movies: IMovie[] = this.toMovieDTO(rawMovies, true);

    movies = await Promise.all(movies.map(async movie => await this.addCommentsToMovie(movie, searchMovieObj.user)));

    movies = await Promise.all(movies.map(async movie => await this.addRatingToMovie(movie, searchMovieObj.user)));


    return movies;
  }

  /**
   * Returns a single movie based on ID, options can be added to add Rating and Comments to movie
   * @param {SearchSingleMovieDTO} searchSingleMovieObj contains movie ID, serch options and user
   */
  async getSingleMovie(searchSingleMovieObj: MovieDTO): Promise<IMovie> {
    const rawMovie = (await this.httpService.get(`http://api.tvmaze.com/shows/${searchSingleMovieObj.id}`).toPromise()).data


    let movie: IMovie = this.toMovieDTO(rawMovie);

    movie = await this.addCommentsToMovie(movie, searchSingleMovieObj.user);
    movie = await this.addRatingToMovie(movie, searchSingleMovieObj.user);

    return movie;
  }

  //HELPERS

  /**
   * Converts raw movie objects to movies
   * @param {any} movie raw movie object to be converted to a movie 
   * @param {boolean} isFromArray flag to filter either single object or array of objects
   */
  private toMovieDTO (movie: any, isFromArray: boolean = false):any  {
    
    if(isFromArray){
      
      return movie.map(item => {

       return {
        id: item.show.id,
        name: item.show.name,
        genres: item.show.genres ,
        runtime: item.show.runtime,
        premiered: item.show.premiered,
        image: item.show.image, 
        summary: item.show.summary,
        officialSite: item.show.officialSite
      }
      })
    }else{

      return {
        id: movie.id,
        name: movie.name,
        genres: movie.genres,
        runtime: movie.runtime,
        premiered: movie.premiered,
        image: movie.image, 
        summary: movie.summary,
        officialSite: movie.officialSite
      }
    }
  }

  /**
   * Returns all comments from a given user for a give movie
   * @param {IMovie} movie Movie object to which comments are added to
   * @param {string} user Needed to display correct comments 
   */
  private async addCommentsToMovie(movie: IMovie, user: string){
    const comments = await this.commentService.getCommentsForMovie(movie.id, user);
    return {comments,...movie}
  }

  /**
   * Returns all ratings for a give movie
   * @param {IMovie} movie Movie object to which ratings are added to
   */
  private async addRatingToMovie(movie: IMovie, owner: string){
    const rating = await this.ratingService.getRatingForMovie(movie.id, owner); 
    
    return {rating,...movie};
  }
}
