import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { RatingDTO } from 'src/DTOs/rating.dto';
import { IRating } from 'src/Interfaces/rating.interface';

@Injectable()
export class RatingService {
  constructor(
    @InjectModel('Rating') private readonly ratingModel: Model<IRating>,
  ) {}

  /**
   * Adds or updates rating for a given movie
   * @param ratingObj contains value of rating, owner and movie ID
   */
  async rateMovie(ratingObj: RatingDTO) {

    await this.ratingModel.updateOne(
      {owner: {$eq: ratingObj.owner},
       movie_id: {$eq: ratingObj.movie_id}},
      {
      value: ratingObj.value,
      owner: ratingObj.owner, 
      movie_id:  ratingObj.movie_id
      },
      {upsert: true}
    )

    return {value: ratingObj.value, movie_id: ratingObj.movie_id}
  };

  /**
   * returns rating for a movie
   * @param movie_id Id of Movie
   */
  async getRatingForMovie(movie_id, owner){
    const rawData = await this.ratingModel.findOne({movie_id: {$eq: movie_id}, owner: {$eq: owner}})
    return rawData ? rawData.value : 0;
  }

}
