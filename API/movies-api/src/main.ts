import { NestFactory } from '@nestjs/core';
import { AppModule } from './Modules/app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as dotenv from 'dotenv';
import { HttpExceptionFilter } from './Middleware/http-filter.filter';

dotenv.config();
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  
  const options = new DocumentBuilder()
  .setTitle('Movie API')
  .setDescription('This is the movie API it adds comments, rating and favorites to MongoDB while fetching all movies from an external API, users and authentications is handled by Auth0 for convenience')
  .setVersion('1.0')
  .addTag('movies')
  .addBearerAuth()
  .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);
  
  app.useGlobalFilters(new HttpExceptionFilter());
  app.enableCors(); 
  await app.listen(`${process.env.PORT}`);
}
bootstrap();
