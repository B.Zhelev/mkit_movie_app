import React from 'react';
import Movies from './Movies'
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import SingleMovie from './SingleMovie';
import Home from './Home';
import AppNavBar from './Navbar';
import PrivateRoute from './ProtectedRoute';
import '../CSS/Movie.css'
import '../CSS/Comment.css'

function App() {
  return (
    <Router>
      <div>
        <AppNavBar />

        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/movies" exact component={Movies} />
          <PrivateRoute path="/movies/:id" component={SingleMovie} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
