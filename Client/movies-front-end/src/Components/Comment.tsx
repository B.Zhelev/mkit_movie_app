import { useAuth0 } from '@auth0/auth0-react';
import React, { useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { deleteComment, editComment } from '../Store/movies.slice';
import { useDispatch } from 'react-redux'
import { withRouter } from 'react-router-dom'
import store from '../Store/store';

const Comment = (props: any) => {
    const comment = props.comment;
    const { id } = props.match.params;
    const [inEdit, setInEdit] = useState(false);
    const [newComment, setNewComment] = useState(comment.comment);
    const { getAccessTokenSilently, user } = useAuth0();
    const dispatch = useDispatch();
    const onSubmit = async (e: any, commentId: string) => {
        e.preventDefault();
        const token = await getAccessTokenSilently();
        setInEdit(false);
        dispatch(editComment({ movie_id: id, comment: newComment, token, commentId,  owner: user.nickname }))
    };
    const editMode = () => { setInEdit(true) };
    const onDelete = async (commentId: string) => {
        const token = await getAccessTokenSilently();
        dispatch(deleteComment({ commentId, movie_id: id, owner: user.nickname, token }))
    }
    const onInput = (e: any) => { setNewComment(e.target.value) }
    return (
        <div className='comment-container'>
            {
                inEdit
                    ? <Form onSubmit={(e) => {onSubmit(e, comment._id)}}>
                        <Form.Group>
                            <Form.Control as="textarea" rows={3} onInput={onInput} value={newComment} />
                        </Form.Group>
                        <Button variant='outline-success' type="submit">
                            Submit
                        </Button>
                    </Form>

                    :
                    <div className='comment-view'>
                        <p>{comment.comment}</p>
                        <hr />
                        <Button variant='outline-dark' onClick={editMode}>Edit</Button>
                        <Button variant='outline-danger'onClick={() => {onDelete(comment._id)}}>Delete</Button>
                    </div>
            }
        </div>
    )
};

export default withRouter(Comment);