import { useAuth0 } from '@auth0/auth0-react';
import React, { useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { postComment } from '../Store/movies.slice';
import { useDispatch } from 'react-redux'
import Comment from './Comment'
import { withRouter } from 'react-router-dom';

const CommentSection = (props: any) => {
    const comments = props.comments;
    const [newComment, setNewComment] = useState('');
    const id = props.match.params.id;
    const { user, getAccessTokenSilently } = useAuth0();
    const onInput = (e: any) => { setNewComment(e.target.value) };
    const dispatch = useDispatch()
    const onSubmit = async (e: any) => {
        e.preventDefault();
        const token = await getAccessTokenSilently();
        dispatch(postComment({ token, comment: newComment, movie_id: id, owner: user.nickname }))
    }

    return (
        <div className='comment-section'>
            <Form onSubmit={onSubmit}>
                <Form.Group>
                    <Form.Control as="textarea" rows={3} onInput={onInput} />
                </Form.Group>
                <Button variant="outline-success" type="submit">
                    Submit
                </Button>
            </Form>
            <hr />
            {comments.map((c: any) => (<Comment comment={c} key={c._id} />))}
        </div>
    )
};

export default withRouter(CommentSection);