import React, {useState} from 'react';
import { Card } from 'react-bootstrap';
import { withRouter } from 'react-router-dom'

const FavoriteMovieCard = (props: any) => {
    const movie = props.movie;
    const onClick = () => {
        props.history.push(`movies/${movie.id}`)
    }
    return (
        <Card className="bg-dark text-white favorite-card" onClick={onClick}>
        <Card.Img className='favorite-card-image' src={movie.image?.medium ? movie.image.medium : 'https://cdn.browshot.com/static/images/not-found.png'} alt={movie.name} />
        <Card.ImgOverlay className='show-favorite-title'>
        <Card.Title>{movie.name}</Card.Title>
        </Card.ImgOverlay>
        </Card>
    )
}

export default withRouter(FavoriteMovieCard);