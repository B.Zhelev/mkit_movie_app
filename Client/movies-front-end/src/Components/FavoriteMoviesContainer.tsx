import React, { useEffect } from 'react';
import { CardColumns, CardDeck, Col, Container, Row } from 'react-bootstrap';
import FavoriteMovieCard from './FavoriteMovieCard';


const FavoriteContainer = ({ movies }: any) => {

useEffect(() => {
    console.log(movies)
})
    return (
        <CardColumns>
            {movies.length &&  movies.map((movie: any) => (<FavoriteMovieCard key={movie.id} movie={movie}/>))}
        </CardColumns>
    )
};

export default FavoriteContainer;