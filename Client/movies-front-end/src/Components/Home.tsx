import { useAuth0 } from '@auth0/auth0-react';
import React, { useEffect } from 'react';
import { Jumbotron, Button, Container } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux'
import { getMovies, searchMovies } from '../Store/movies.slice';
import store from '../Store/store';
import { fetchFavorites, getFavorites } from '../Store/user.slice';
import FavoriteMovieContainer from './FavoriteMoviesContainer'

const Home = (props: any) => {
    const { isAuthenticated, getAccessTokenSilently, user, loginWithRedirect } = useAuth0();
    const dispatch = useDispatch();
    const favorites  = useSelector((getFavorites));
    const setFavorites = async () => {
        const token = await getAccessTokenSilently();
        const name = user.nickname;
        dispatch(fetchFavorites({ token, user: name }))
    }
    const signUp = () => {loginWithRedirect()};
    const searchQuery = async (e: any) => {
        e.preventDefault();
        if (isAuthenticated) {
          const token = await getAccessTokenSilently();
          const name = user.nickname;
          dispatch(searchMovies({ searchTerm: 'Avengers', token, user: name }))
        } else {
          dispatch(searchMovies({ searchTerm: 'Avengers' }))
        }
    
        props.history.push('/movies')
      }
    useEffect(() => {
        if (!isAuthenticated) return;
        setFavorites();
    }, [isAuthenticated])

    return (
        <Container fluid>
            <Jumbotron className="header">
                <h1>Welcome to your new Movie-Journal!</h1>
                <p>
                    {isAuthenticated ? 
                        `You can see your favorite movies bellow,
                        Click on one and it will take you to that movie`
                        :
                        `Please make an account so that you can access the full features` 
                    }
                </p>
                <p>
                    {isAuthenticated ? <Button onClick={searchQuery} variant="light">Search</Button> : <Button onClick={signUp} variant="light">Register</Button>}
                </p>
            </Jumbotron>
            {isAuthenticated && <FavoriteMovieContainer movies={favorites} />}
        </Container>

    )
};

export default Home;