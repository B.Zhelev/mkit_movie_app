import { useAuth0 } from '@auth0/auth0-react';
import React, { useState, useEffect } from 'react';
import { Button, Image, Container, Row, Col } from 'react-bootstrap';
import store from '../Store/store';
import { useSelector, useDispatch } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { addToFavorites, removeFromFavorites, getFavoriteIds } from '../Store/user.slice';

const MovieCard = (props: any) => {
  const singleView = props.singleView;
  const movie = props.movie;
  const { isAuthenticated, getAccessTokenSilently, user } = useAuth0();
  const dispatch = useDispatch();
  const favoritesIds = useSelector(getFavoriteIds)
  const goToMovie = (id: any) => {
    if(!singleView){
      props.history.push(`movies/${id}`)
    }
  }
  const appAddToFavorites = async (id: number) => {
    const token = await getAccessTokenSilently();
    const owner = user.nickname;
    dispatch(addToFavorites({ id, token, owner }));
  }

  const appRemoveFromFavorites = async (id: any) => {
    const token = await getAccessTokenSilently();
    const owner = user.nickname;
    dispatch(removeFromFavorites({ id, token, owner }));
  }

  return (
    <Container>
      <Row>
        <Col sm={12} md={6} className="movie-card">
          <Image fluid src={movie.image?.original ? movie.image.original : 'https://cdn.browshot.com/static/images/not-found.png'} alt={movie.name} onClick={() => { goToMovie(movie.id) }}/>
        </Col>
        <Col sm={12} md={6}>
          <div className='single-movie-description'>
          <h3 onClick={() => { goToMovie(movie.id) }}>{movie.name} {movie.premiered && `(${movie.premiered.split('-')[0]})`}</h3>
          <p>{movie?.genres?.join(' ')} | {movie?.runtime} minutes</p>
          <div dangerouslySetInnerHTML={{ __html: movie?.summary }} />
          {movie.officialSite && <a className="movie-site" href={movie?.officialSite}>Visit Official Site</a>}
          {isAuthenticated && (favoritesIds.includes(movie.id) ?
            <Button onClick={() => { appRemoveFromFavorites(movie.id) }} variant='outline-danger'> Remove From Favorites </Button> :
            <Button onClick={() => { appAddToFavorites(movie.id) }} variant='outline-success'> Add To Your Favorites </Button>)}
        </div>
        </Col>
      </Row>
    </Container>
  )
};

export default withRouter(MovieCard);