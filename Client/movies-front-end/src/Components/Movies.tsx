import React from 'react'
import { getMovies } from '../Store/movies.slice';
import { useSelector } from 'react-redux';
import MovieCard from './MovieCard'
function MoviePage() {
  const movies = useSelector(getMovies)
  return (
    <div className='search-result-container'>
      {movies.map((movie: any) => (
        <MovieCard key={movie.id} movie={movie} />
      ))}
    </div>
  );
}

export default MoviePage