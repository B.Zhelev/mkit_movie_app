import { useAuth0 } from '@auth0/auth0-react';
import React, { useState } from 'react';
import { withRouter } from 'react-router-dom'
import { Navbar, Nav, Form, FormControl, Button } from 'react-bootstrap';
import { searchMovies } from '../Store/movies.slice';
import store from '../Store/store';

const AppNavBar = (props: any) => {

  const { isAuthenticated, loginWithRedirect, logout, user, getAccessTokenSilently } = useAuth0();
  const [search, setSearch] = useState('');
  const onInput = (e: any) => {
    setSearch(e.target.value);
  }
  const searchQuery = async (e: any) => {
    e.preventDefault();
    if (isAuthenticated) {
      const token = await getAccessTokenSilently();
      const name = user.nickname;
      store.dispatch(searchMovies({ searchTerm: search, token, user: name }))
    } else {
      store.dispatch(searchMovies({ searchTerm: search }))
    }

    props.history.push('/movies')
  }
  const onKeyDown = async (e: any) => {
    if (e.key === 'Enter' && e.shiftKey === false) {
      e.preventDefault();
      if (isAuthenticated) {
        const token = await getAccessTokenSilently();
        const name = user.nickname;
        store.dispatch(searchMovies({ searchTerm: search, token, user: name }))
      } else {
        store.dispatch(searchMovies({ searchTerm: search }))
      }

      props.history.push('/movies')
    } else {
      onInput(e)
    }
  }
  const goToHome = () => {
    props.history.push('/')
  }
  const appLogin = () => { loginWithRedirect() };
  const appLogout = () => { logout() };

  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand onClick={goToHome}>Movie-Journal</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          {isAuthenticated
            ?
            <Nav.Link onClick={appLogout}>Logout</Nav.Link>
            :
            <Nav.Link onClick={appLogin}>Login</Nav.Link>}

        </Nav>
        <Form inline onKeyDown={onKeyDown}>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" onInput={onInput} />
          <Button onClick={searchQuery} variant="outline-success">Search</Button>
        </Form>
      </Navbar.Collapse>
    </Navbar>
  )
}

export default withRouter(AppNavBar);