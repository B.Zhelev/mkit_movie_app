import { useAuth0 } from '@auth0/auth0-react';
import React from 'react';
import { Route, Redirect } from 'react-router-dom'
const PrivateRoute = ({ component: Component, ...rest }: any) => {

  const { isAuthenticated } = useAuth0();

  return (

    <Route {...rest} render={(props) => (
      isAuthenticated
        ? <Component {...props} />
        : <Redirect to='/' />
    )} />
  )
}

export default PrivateRoute;