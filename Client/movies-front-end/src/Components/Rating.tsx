import { useAuth0 } from '@auth0/auth0-react';
import React from 'react';
import ReactStars from 'react-rating-stars-component';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom'
import { rateMovie, selectMovieRating } from '../Store/movies.slice';

const Rating = (props: any) => {

    const { id } = props.match.params;
    const dispatch = useDispatch();
    const rating = useSelector((state: any) => selectMovieRating(state, id))
    const { user, getAccessTokenSilently } = useAuth0();
    let newRating = rating;
    const ratingChanged = async (e: number) => {
        const token = await getAccessTokenSilently();
        const owner = user.nickname;
        newRating = e;
        dispatch(rateMovie({ token, owner, id, value: newRating }))
    };
    return (
        <div>
            <h1>Review</h1>
            <ReactStars
                value={rating}
                count={5}
                onChange={ratingChanged}
                size={50}
                isHalf={true}
                emptyIcon={<i className="far fa-star"></i>}
                halfIcon={<i className="fa fa-star-half-alt"></i>}
                fullIcon={<i className="fa fa-star"></i>}
                activeColor="#ffd700"
            />
        </div>
    )
}

export default withRouter(Rating)