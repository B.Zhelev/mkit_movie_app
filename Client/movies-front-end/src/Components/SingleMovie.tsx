import { useAuth0 } from '@auth0/auth0-react';
import React, { useEffect, useState } from 'react';
import { useSelector,useDispatch} from 'react-redux'
import { IMovie } from '../Interfaces/movies';
import { getMovie, selectMovieById, selectMovieComments } from '../Store/movies.slice';
import Rating from './Rating'
import CommentSection from './CommentSection';
import MovieCard from './MovieCard'

const SingleMovie = (props: any) => {
  const { id } = props.match.params;
  const { getAccessTokenSilently, user } = useAuth0();
  const dispatch = useDispatch();
  const movie = useSelector((state) => selectMovieById(state, id)) as IMovie
  const comments = useSelector((state) => selectMovieComments(state, id))
  const appSetMovie = async () => {

    if (!movie) {
      const token = await getAccessTokenSilently();
      const name = user?.nickname;
      dispatch(getMovie({ id, token, user: name }))
    }
  }
  useEffect(() => {
    appSetMovie();
  }, []);

  return (
    <div className='single-movie-container'>
      <MovieCard movie={movie} singleView={true}/>
      <Rating />
      <CommentSection comments={comments} />
    </div>
  )
}

export default SingleMovie;