export interface IComment {

  comment: string;
  readonly owner: string;
  readonly movie_id: number;
  readonly _id: string;
}

export interface IPostComment {
  comment: string;
  movie_id: number;
  owner: string;
  token: string
}

export interface IUpdateComment {
  commentId: string;
  comment?: string;
  movie_id: number;
  token: string;
  owner: string
}
