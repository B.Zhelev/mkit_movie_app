import { IComment } from "./comment";

export interface InitMovieState {
    status: string;
};

export interface ISearchSingleMovie {
    id: number;
    token?: string;
    user?: string;
};

export interface ISearchMovies {
    searchTerm: string;
    token?: string;
    user?: string;
};



export interface IMovie {
    readonly id: number;
    readonly name: string;
    readonly genres: string[];
    readonly runtime: number;
    readonly premiered: string;
    readonly officialSite: string | null;
    readonly image: Object;
    readonly summary: string;
    readonly comments: IComment[];
    rating: number;
}