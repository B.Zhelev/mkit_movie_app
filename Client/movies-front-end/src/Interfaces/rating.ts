export interface IRating {
    value: number;
    id: number;
    owner: string;
    token: string
}