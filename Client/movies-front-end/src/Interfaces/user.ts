import { IMovie } from './movies'
export interface InitUserState {
    favorites: IMovie[];
}

export interface IGetFavorites {
    token: string;
    user: string;
}

export interface IChangeFavorites {
    id: number;
    owner: string;
    token: string;
}