import { IPostComment, IUpdateComment } from "../Interfaces/comment";
import { IMovie, ISearchMovies, ISearchSingleMovie } from "../Interfaces/movies";
import { IRating } from "../Interfaces/rating";
import { IChangeFavorites, IGetFavorites } from "../Interfaces/user";

const BASE_PATH = 'http://localhost:3000';
const SEARCH_MOVIES_PATH = (term: string, user?: string) => {
    let retVal = `${BASE_PATH}/movies?searchTerm=${term}`;
    if (user) retVal += `&user=${user}`;

    return retVal;
};
const SEARCH_SINGLE_MOVIE_PATH = (id: number, user?: string) => {
    let retVal = `${BASE_PATH}/movies/${id}`;
    if (user) retVal += `?user=${user}`;

    return retVal;
};
const COMMENT_PATH = (id: number) => `${SEARCH_SINGLE_MOVIE_PATH(id)}/comment`;
const RATE_PATH = (id: number) => `${SEARCH_SINGLE_MOVIE_PATH(id)}/rate`;
const FAVORITE_PATH = (id: number) => `${SEARCH_SINGLE_MOVIE_PATH(id)}/favorites`;
const USER_PATH = (userName: string) => `${BASE_PATH}/user/${userName}`;

const HEADERS = (token?: string) => {
    return {
        'Content-Type': 'application/json',
        authorization: `Bearer ${token}`
    }
};

const _data = (body: any, method: string, token?: string) => {

    return {
        method,
        body: JSON.stringify(body),
        headers: HEADERS(token)
    }
}
export default {
    //MOVIES
    async searchAllMovies(req: ISearchMovies): Promise<IMovie[]> {
        return (await fetch(SEARCH_MOVIES_PATH(req.searchTerm, req?.user), { headers: HEADERS(req?.token) })).json();

    },
    async getSingleMovie(req: ISearchSingleMovie): Promise<IMovie> {
        return (await fetch(SEARCH_SINGLE_MOVIE_PATH(req.id, req?.user), { headers: HEADERS(req?.token) })).json()
    },
    //COMMENTS
    async postComment(req: IPostComment) {
        const body = { comment: req.comment, owner: req.owner, movie_id: req.movie_id};

        return (await fetch(COMMENT_PATH(req.movie_id), _data(body, 'POST', req.token))).json()
    },
    async editComment(req: IUpdateComment) {
        const body = { commentId: req.commentId, comment: req.comment, movie_id: req.movie_id, owner: req.owner };
        return (await fetch(COMMENT_PATH(req.movie_id), _data(body, 'PUT', req.token))).json();
    },
    async deleteComment(req: IUpdateComment) {
        const body = { commentId: req.commentId, comment: req.comment, movie_id: req.movie_id, owner: req.owner };
        return (await fetch(COMMENT_PATH(req.movie_id), _data(body, 'DELETE', req.token))).json()
    },
    //RATE
    async rateMovie(RateObj: IRating) {
        const body = { value: RateObj.value, owner: RateObj.owner, movie_id: RateObj.id };
        return (await fetch(RATE_PATH(RateObj.id), _data(body, 'POST', RateObj.token))).json()
    },
    //FAVORITES
    async addToFavorites(req: IChangeFavorites) {
        const body = { owner: req.owner, movie_id: req.id  };

        return (await fetch(FAVORITE_PATH(req.id), _data(body, 'POST', req.token))).json()
    },
    async removeFromFavorites(req: IChangeFavorites) {
        const body = { owner: req.owner, movie_id: req.id };
        console.log(body)
        return (await fetch(FAVORITE_PATH(req.id), _data(body, 'DELETE', req.token))).json()
    },
    async getFavorites(req: IGetFavorites) {

        return (await fetch(USER_PATH(req.user), { headers: HEADERS(req.token) })).json()
    }
}



