import {
  createSlice,
  createAsyncThunk,
  createEntityAdapter,
  createSelector,
} from '@reduxjs/toolkit';
import { IPostComment, IUpdateComment } from '../Interfaces/comment';
import { IMovie,  ISearchMovies, ISearchSingleMovie } from '../Interfaces/movies';
import { IRating } from '../Interfaces/rating';

import client from '../Requests/requests';
import { fetchFavorites } from './user.slice';


const movieAdapter = createEntityAdapter({
  selectId: (movie: IMovie) => movie.id
});

const initialState = movieAdapter.getInitialState({
  status: 'idle'
});



// Thunk functions
export const searchMovies = createAsyncThunk('movies/searchMovies', async (request: ISearchMovies) => {

  return await client.searchAllMovies(request);
});

export const getMovie = createAsyncThunk('movies/getMovie', async (request: ISearchSingleMovie) => {

  return await client.getSingleMovie(request);
});

export const postComment = createAsyncThunk('movies/testPostComment', async (request: IPostComment) => {

  return await client.postComment(request)
})

export const editComment = createAsyncThunk('movies/editComment', async (request: IUpdateComment) => {

  return await client.editComment(request)
})

export const deleteComment = createAsyncThunk('movies/deleteComment', async (request: IUpdateComment) => {

  return await client.deleteComment(request)
})

export const rateMovie = createAsyncThunk('movies/rateMovie', async (request: IRating) => {

  return await client.rateMovie(request)
})

const moviesSlice = createSlice({
  name: 'movies',
  initialState,
  reducers: {
    addMovie: movieAdapter.addOne,
    addComment(state, action) {
      const req: IPostComment = action.payload;
      state.entities[req.movie_id]?.comments?.push(action.payload)
    },
    patchComment(state, action) {
      const req = action.payload;
      const movie = state.entities[req.movie_id] as IMovie;
      const commentIndex = movie.comments.findIndex(c => c._id == req._id);
      movie.comments[commentIndex] = {
        owner: movie.comments[commentIndex].owner,
        _id: req.commentId,
        comment: req.comment,
        movie_id: req.movie_id
      };
      state.entities[req.movie_id] = movie;
    },
    delComment(state, action) {
      const {commentId, movie_id} = action.payload;
      const movie = state.entities[movie_id];
      const commentIndex = movie?.comments.findIndex(c => c._id == commentId);
      movie?.comments.splice(commentIndex as number, 1);      
      state.entities[movie_id] = movie;
    },
    rateStateMovie(state, action) {
      const req = action.payload;
      const movie = state.entities[req.movie_id] as IMovie
      movie.rating = req.value;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(postComment.fulfilled,(state, action) => {moviesSlice.caseReducers.addComment(state,action)})
      .addCase(editComment.fulfilled, (state, action) => {moviesSlice.caseReducers.patchComment(state, action)})
      .addCase(deleteComment.fulfilled, (state, action) => {moviesSlice.caseReducers.delComment(state, action)})
      .addCase(fetchFavorites.fulfilled, movieAdapter.upsertMany)
      .addCase(searchMovies.fulfilled, (state, action) => {
        movieAdapter.removeMany(state, state.ids);
        movieAdapter.upsertMany(state, action.payload);
      })
      .addCase(getMovie.fulfilled, (state, payload ) => {moviesSlice.caseReducers.addMovie(state, payload)})
      .addCase(rateMovie.fulfilled, (state, action) => {moviesSlice.caseReducers.rateStateMovie(state, action)})
  }
})

export default moviesSlice.reducer

export const {
  selectById: selectMovieById,
  selectAll: getMovies,
} = movieAdapter.getSelectors((state: any) => state.movie)

export const selectMovieComments = createSelector(
  selectMovieById,
  (movie) => movie?.comments
)

export const selectMovieRating = createSelector(
  selectMovieById,
  (movie) => movie?.rating
)