import { configureStore } from '@reduxjs/toolkit'
import MoviesReducer from './movies.slice'
import UserReducer from './user.slice'

const store = configureStore({
  reducer: {
    // Define a top-level state field named `todos`, handled by `todosReducer`
    movie: MoviesReducer,
    user: UserReducer
  },
})

export default store
