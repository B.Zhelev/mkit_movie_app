import {
  createSlice,
  createAsyncThunk,
  createSelector,
  createEntityAdapter
} from '@reduxjs/toolkit';
import { IMovie } from '../Interfaces/movies';
import { IChangeFavorites, IGetFavorites, InitUserState, } from '../Interfaces/user';

import client from '../Requests/requests';

const userAdapter = createEntityAdapter({
  selectId: (movie: IMovie) => movie.id
})

const initialState = userAdapter.getInitialState({
})

// Thunk functions
export const fetchFavorites = createAsyncThunk('user/fetchFavorites', async (request: IGetFavorites) => {

  return await client.getFavorites(request);
});

export const addToFavorites = createAsyncThunk('user/addToFavorites', async (request: IChangeFavorites) => {

 return await client.addToFavorites(request);
})

export const removeFromFavorites = createAsyncThunk('user/removeFromFavorites', async (request: IChangeFavorites) => {

  return await client.removeFromFavorites(request);
})

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchFavorites.fulfilled,userAdapter.upsertMany)
      .addCase(addToFavorites.fulfilled, userAdapter.addOne)
      .addCase(removeFromFavorites.pending, (state, action) => {
      })
      .addCase(removeFromFavorites.fulfilled, (state,action) => {userAdapter.removeOne(state, action.payload.id)})
  },  
})
export const {
  selectAll: getFavorites
} = userAdapter.getSelectors((state: any) => state.user);


export const getFavoriteIds = createSelector(
  getFavorites,
  (favorites) => favorites.map(f => f.id)
)

export default userSlice.reducer;