import React from 'react';
import ReactDOM from 'react-dom';
import App from './Components/App';
import reportWebVitals from './reportWebVitals';
import { Auth0Provider } from "@auth0/auth0-react";
import { Provider } from 'react-redux'
import store from '../src/Store/store'
import { searchMovies } from '../src/Store/movies.slice';

// store.dispatch(searchMovies({searchTerm: 'Avengers'}))

ReactDOM.render(
  <Auth0Provider
    domain="dev-y6jrgj0b.eu.auth0.com"
    clientId="SANvMgPd0ynhKMpKf0XxUp0AeVz96eI0"
    redirectUri={window.location.origin}
    audience="http://localhost:3000"
  >
    <Provider store={store}>
     <App />
    </Provider>
  </Auth0Provider>,
  document.getElementById("root")
);
