This is the 7 day project for MKIT

API: 
        Due to time restrictions validation needs to be added, functions are anotated using JSDoc. 
        Built with NestJS, MongoDB, TypeScript and Auth0

        UPDATE: Added proper documentation with swagger, it is available at localhost:3000/api (Added after submition)


Client: 
        Built with React, TypeScript, Bootstrap, Redux Toolkit and Auth0; 


    Setup: 
        First create a .env folder in API with the following params: 
            API: 
                AUTH0_DOMAIN=dev-y6jrgj0b.eu.auth0.com (This is my domain you can use it as it is already set)

                AUTH0_AUDIENCE=http://localhost:3000/api (This is my audience you can use it as it is already set)

                MONGO_PASS (ADD)

                MONGO_USR_NAME (ADD)

                MONGO_APP (ADD)

                PORT=3000 (Do not change this as Auth0 will stop working)
            
            Client: 
                I have not added an ENV file as it would contain only the Auth0 config which is already shared with you;

                Nevertheless the config is at index.ts

        Second run `npm i` in ./API/movie-api and ./Client/movies-front-end 

        Third start API with npm run start:dev (for watch mode) and start the Client with `npm start`
